/**
 * ArrayWriter: Sincroniizacao de threads
 * Exemplo que mostra um problema na
 * escrita compartilhada de um vetor
 * por multiplas threads.
 * 
 * Autor: Bruno Russi Lautenschlager
 * Ultima modificacao: 09/08/2017
 */
package exemplo3;

import java.util.*;

public class VetorCompartilhado {

    private int[] array;
    private int writeIndex=0; //indice do proximo elemento a ser gravado
    private static Random generator = new Random();
    
    public VetorCompartilhado(int size){
        array = new int[size];
    }//fim do construtor
    
    public synchronized void add(int value){
        
        int position = writeIndex;
        try {
            //Coloca a thread para dormir entre 0 e 500ms
            Thread.sleep(generator.nextInt(500));
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        
        //Insere um valor na posicao do vetor
        array[position] = value;
        System.out.printf("\n%s escreveu %2d na posicao %d", Thread.currentThread().getName(), value,position);
        
        //Incrementa o indice
        writeIndex++;
        System.out.printf("\nProximo indice: %d", writeIndex);
    }
    
    
    public synchronized void sub(){
        
        int position = writeIndex;
        try {
            //Coloca a thread para dormir entre 0 e 500ms
            Thread.sleep(generator.nextInt(500));
        } catch (InterruptedException e){
            e.printStackTrace();
        }
               
        int elemento = generator.nextInt(array.length-1);
        int[] arrayCp = new int[array.length-1];
        
        for (int i=0; i<array.length; i++){
            if(array[i] > 0){
                arrayCp[i] = array[i];
            }
        }
        
        array = arrayCp;
        
    }
    //Metodo sobrecarregado
    public String toString(){
        return "\nVetor:\n" + Arrays.toString(array);
    }
    
}
