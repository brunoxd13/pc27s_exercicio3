/**
 * ArrayWriter: Sincroniizacao de threads
 * Exemplo que mostra um problema na
 * escrita compartilhada de um vetor
 * por multiplas threads.
 * 
 * Autor: Bruno Russi Lautenschlager
 * Ultima modificacao: 09/08/2017
 */
package exemplo3;

import java.util.*;


public class ArrayWriter implements Runnable {

    private final VetorCompartilhado vetorCompartilhado;
    private int startValue; 
    private static Random generator = new Random();
        
    public ArrayWriter(int value, VetorCompartilhado vetor){
        startValue=value;
        vetorCompartilhado=vetor;
    }//fim do construtor
    
    public void run(){
        
        for (int i=startValue; i<startValue+3; i++){
//            vetorCompartilhado.add(i);
            if( generator.nextInt(500) % 2 == 0){
                vetorCompartilhado.add(i); //Adiciona um elemento em ordem crescente no vetor compartilhado
            }else{
                vetorCompartilhado.sub(); //Remove um elemento em ordem crescente no vetor compartilhado
            }
                      
        }
        
    }//fim run
    
}
